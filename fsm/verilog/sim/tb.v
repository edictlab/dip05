module tb;
reg clk;
reg [1:0] in;
wire [1:0] out;
reg e = 1'b1;
register #(.WIDTH(2)) r1(.clk(clk),.d(in),.en(e),.q(out));
initial
  clk = 0;
always
  #2 clk = ~clk;
initial begin
  $monitor("t=%d, in=%b, out=%b",$time,in,out);
  $dumpvars(0,tb);
  in = 2'd2;
  #3 in = 2'd3;
  #4 in = 2'd0;
  #6 $finish;
end
endmodule
