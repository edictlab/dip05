module semafor( clk, dp, dpled, sa, sp, clkled);
input clk;
input dp;
output dpled;
output reg [2:0] sa;
output reg [1:0] sp;
output reg clkled;

parameter [1:0] S0 = 2'b00;
parameter [1:0] S1 = 2'b01;
parameter [1:0] S2 = 2'b10;
parameter [1:0] S3 = 2'b11;

parameter [2:0] SAR = 3'b001;
parameter [2:0] SAY = 3'b010;
parameter [2:0] SARY = 3'b011;
parameter [2:0] SAG = 3'b100;
parameter [1:0] SPR = 2'b01;
parameter [1:0] SPG = 2'b10;

reg [1:0] state;
reg [1:0] next_state;

// 1: (* buffer_type = "bufg" *)
// 2: wire new_clk /* synthesis buffer_type="bufg" */ ;
wire new_clk /* synthesis buffer_type="bufg" */ ;

///* synthesis buffer_type="bufg" */ ;
// 3: synthesis attribute buffer_type of new_clk is bufg
// 4: instanciranjem BUFG bloka

clock5s clock_div(clk, new_clk);
//assign new_clk = clk5s;

always @(posedge new_clk)
  clkled <= !clkled;

initial begin
  state <= S0;
  next_state <= S0;
end

assign dpled = dp;

always @ (posedge new_clk)
begin
  state <= next_state;
end

always @(*)begin
  case(state)
    S0: 
      if(dp == 1'b1) next_state = S1;
      else next_state = S0;
    S1: 
      next_state = S2;
    S2: 
      if(dp == 1'b1) next_state = S2;
      else next_state = S3;
    S3: 
      next_state = S0;
    default:
      next_state = S0;
  endcase
end

//always @(posedge new_clk)
always @(posedge new_clk)
begin
  case(state)
    S0: begin
      sa <= SAG;
      sp <= SPR; 
    end
    S1: begin
      sa <= SAY;
      sp <= SPR; 
    end
    S2: begin
      sa <= SAR;
      sp <= SPG; 
    end
    S3: begin
      sa <= SARY;
      sp <= SPR; 
    end
    default: begin
      sa <= SAG;
      sp <= SPR; 
    end
  endcase
end

endmodule

//-------------------------------
module clock5s(clk_in, clk_out);
input clk_in;
output reg clk_out=0;
reg [29:0] counter = 0;
always @(posedge clk_in)
  if(counter < 250000000)
    counter <= counter + 1;
  else begin
    counter <= 0;
    clk_out <= ~ clk_out;
  end

// BUFG BUFG_instance(.O(clk_out), .I(counter[28]) );
// assign clk_out = counter[28];
endmodule

