
module vgapr(clk, dugme, Hsync, Vsync, vgaRed, vgaGreen, vgaBlue);
    input clk, dugme;
    output Hsync, Vsync;
    output reg [2:0] vgaRed;
    output reg [2:0] vgaGreen;
    output reg [1:0] vgaBlue;

    wire vga_clk;
    wire inDisplayArea;

    wire [11:0] x;   // x coordinate
    wire [11:0] y;   // y coordinate

    clock_divider #(.TAKE_BIT(1)) vga_clock( clk, vga_clk);

    vga640x480(
      .dclk       (vga_clk),
      .clr        (dugme),
      .hsync      (Hsync),
      .vsync      (Vsync),
      .x          (x),
      .y          (y),
      .inrange    (inDisplayArea)
    );

    always @(posedge vga_clk)
    begin
      if(inDisplayArea)
      begin
        if( (y==50 || y==350) && (50<=x && x<=550)
          || (x==50 || x==550) && (50<=y && y<=350))
        begin
          vgaRed    <= 3'b111;
          vgaGreen  <= 3'b111;
          vgaBlue   <= 2'b11;
        end
        else if(x<y) begin
          vgaRed    <= 3'b000;
          vgaGreen  <= 3'b111;
          vgaBlue   <= 2'b11;
        end
        else begin
          vgaRed    <= 3'b000;
          vgaGreen  <= 3'b000;
          vgaBlue   <= 2'b11;
        end
      end
      else
      begin
        vgaRed    <= 3'b000;
        vgaGreen  <= 3'b000;
        vgaBlue   <= 2'b00;
      end
    end




endmodule

// ##### clock divider #################
module clock_divider(clk_in, clk_out);
parameter TAKE_BIT = 0;
  input clk_in;
  output wire clk_out;

  reg [31:0] counter;

  initial counter = 32'b0;
  always @(posedge clk_in)
  begin
    counter = counter + 1;
  end

  assign clk_out = counter[TAKE_BIT];
endmodule
