
// ###### VGA 640x480 ##############

module vga640x480(
	input wire dclk,			//pixel clock: 25MHz
	input wire clr,			//asynchronous reset
	output wire hsync,		//horizontal sync out
	output wire vsync,		//vertical sync out
  output reg [11:0] x,   // x coordinate
  output reg [11:0] y,   // y coordinate
  output wire inrange
	);

// video structure constants
parameter hpixels = 800;// horizontal pixels per line
parameter vlines = 521; // vertical lines per frame
parameter hpulse = 96; 	// hsync pulse length
parameter vpulse = 2; 	// vsync pulse length
parameter hbp = 144; 	// end of horizontal back porch
parameter hfp = 784; 	// beginning of horizontal front porch
parameter vbp = 31; 		// end of vertical back porch
parameter vfp = 511; 	// beginning of vertical front porch
// active horizontal video is: 784 - 144 = 640
// active vertical video is: 511 - 31 = 480

// Brojaci: horizontalni i vertikalni
reg [11:0] hc;
reg [11:0] vc;

// Brojaci: horizontalni i vertikalni inkrementiranje (sekvencijalna logika)
always @(posedge dclk or posedge clr)
begin
	// reset 
	if (clr == 1)
	begin
		hc <= 0;
		vc <= 0;
	end
	else
	begin
		if (hc < hpixels - 1)
			hc <= hc + 1;
		else
    begin
			hc <= 0;
			if (vc < vlines - 1)
				vc <= vc + 1;
			else
				vc <= 0;
		end
		
	end
end

// Signal za hor. i vert. sinhronizaciju
assign hsync = (hc < hpulse) ? 0:1;
assign vsync = (vc < vpulse) ? 0:1;

wire inrange_y, inrange_x;
assign inrange = inrange_x & inrange_y;
assign inrange_x = (hc >= hbp) && (hc < hbp + 640);
assign inrange_y = (vc >= vbp) && (vc < vfp);

// pozicija x,y u vidljivom dijelu
always @(posedge dclk)
begin
  if(clr)
  begin
    x <= 0;
    y <= 0;
  end
  else
  begin
    if (inrange_y)
      y <= vc - vbp;
    else
      y <= -1;

    if(inrange_x)
      x <= hc - hbp;
    else
      x <= -1;
  end
end

endmodule


